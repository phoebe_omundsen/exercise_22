﻿using System;
using System.Collections.Generic;

namespace exercise_22
{
    class Program
    {
        static void Main(string[] args)
        {
        Console.Clear();
        //Task A
        //Declare variables
        var movieTitles = new String[5] {"Donnie Darko", "Up", "The Neverending Story", "Battle Royale", "Richie Rich"};

        //Print each movie title to screen on new line
        Console.WriteLine("========== TASK A ==========");
        Console.WriteLine(movieTitles[0]);
        Console.WriteLine(movieTitles[1]);
        Console.WriteLine(movieTitles[2]);
        Console.WriteLine(movieTitles[3]);
        Console.WriteLine(movieTitles[4]);

        //Task B - Change the 1st and 3rd titles 
        movieTitles[0] = "S Darko";
        movieTitles[2] = "Peter Pan";

        //Task C - Sort the array in alphabetical order
        Array.Sort(movieTitles);

        //Task D - Display on screen length of array
        Console.WriteLine();
        Console.WriteLine("========== TASK D ==========");
        Console.WriteLine($"The amount of movie titles in the array = {movieTitles.Length}");

        //Task E - Display on screen the whole string of arrays
        Console.WriteLine();
        Console.WriteLine("========== TASK E ==========");
        Console.WriteLine(string.Join(", ", movieTitles));
        Console.WriteLine();

        
        //Task F - good places to eat - Create new List<T> 5 places
        var placesEat = new List<string> {"Macau", "Mount Sushi", "Burgerfuel", "Patio Rose", "MrMyagi"};
        //Task G - Sort in alphabetical order - print to screen
        placesEat.Sort();
        Console.WriteLine("========== TASK G ==========");
        Console.WriteLine(string.Join(", ", placesEat));
        Console.WriteLine();

        //Task H - remove 3rd space and display remaining 4
        placesEat.Remove(placesEat[2]);
        Console.WriteLine("========== TASK H ==========");
        Console.WriteLine(string.Join(", ", placesEat));

        //End program with enter key
        Console.WriteLine();
        Console.WriteLine("Press <Enter> to end program");
        Console.ReadKey();


        }
    }
}
